<nav class="top-bar" style="">
  <ul class="title-area">
    <!-- Title Area -->
    <li class="name">
    </li>
    <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
    <li class="toggle-topbar menu-icon"><a href=""><span>Menu</span></a></li>
  </ul>

          
  <section class="top-bar-section">
      <ul class="left">
        <li class="divider"></li>
        <li class="hover"><a href="#">HOME</a></li>
        <li class="divider"></li>
        <li class="hover"><a href="#">ABOUT</a></li> 
        <li class="divider"></li>
        <li class="hover"><a href="#">ACADEMICS</a></li>
        <li class="divider"></li>
        <li class="hover"><a href="#">ADMISSIONS</a></li>
        <li class="divider"></li>
        <li class="hover"><a href="#">LIFE ON CAMPUS</a></li>
        <li class="divider"></li>
        <li class="hover"><a href="#">ATHLETICS</a></li>
        <li class="divider"></li>
        <li class="hover"><a href="#">NEWS & EVENTS</a></li>
        <li class="divider"></li>
        <li class="hover"><a href="#">CENTERS</a></li>
        <li class="divider"></li>
      </ul>
  </section>
</nav>
