




  <!-- Masthead -->
  <?php $app->file_include('components/masthead.php'); ?>


  <div class="row">
    <div class="tiny-12 small-12 columns page-title">
      <span class="">For Parents & Families</span> 
      <a href="#" class="small round button act-now">Make a Donation</a>
    </div>
  </div>  


  <div class="row featured">
    <div class="tiny-12 small-6 columns featured-media">
      <img src="http://placehold.it/450x232&text=Featured Media"/> 
    </div>
    <div class="tiny-12 small-6 columns this-week">
      <div class="row">
        <div class="tiny-12 columns this-week-title">
          <h3>This Week</h3>
          <div class="weather hide-for-tiny">
            <?php $app->file_include('blocks/gateway/weather.php'); ?>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="tiny-12 small-6 columns recent-news">
          <div>
            Recent news
            <ul class="news-list no-bullets teasers">
              <li class="">  
                  <a href="#">UNC Asheville Named a Best Buy by Fiske Guide to Colleges</a>
              </li>
              <li class="">  
                <a href="#">Impractical Laborers Open Conference, Exhibition July 6 at Center for Craft, Creativity &amp; Design</a>
              </li>
            </ul>
            <p><a href="#">More News &gt;</a></p>
          </div>
        </div>
        <div class="tiny-12 small-6 columns events-list">
          Events
          <ul class="no-bullets">
          <li class="">
            <div class="row">
              <div class="tiny-3 small-4 large-3 columns">
                <a class="calendar-graphic" href="#">
                 <span class="date-month">Jul</span> 
                 <span class="date-day">4</span>
                </a> 
              </div>
              <div class="tiny-9 small-8 large-9 columns">
                <a class="event-details" href="#">
                  <span class="date-time">3:00 PM</span>Financial Aid Workshop</a>
                  <div class="show-for-tiny">
                    Turpis Etiam In Sagittis nunc Pellentesque Nec Dapibus Nisl Non Feugiat Purus
                  </div>
              </div>
            </div>
          </li>
          <li class="">
            <div class="row">
              <div class="tiny-3 small-4 large-3 columns">
                <a class="calendar-graphic" href="#">
                 <span class="date-month">Jul</span> 
                 <span class="date-day">5</span>
                </a> 
              </div>
              <div class="tiny-9 small-8 large-9 columns">
                <a class="event-details" href="#">
                  <span class="date-time">7:30 PM</span>New Students Reception and Dance Party</a>
                  <div class="show-for-tiny">
                    Turpis Etiam In Sagittis nunc Pellentesque Nec Dapibus Nisl Non Feugiat Purus
                  </div>
              </div>
            </div>
          </li>
          </ul>
          <p><a href="#">More Events &gt;</a></p>
        </div>
      </div>
    </div>
  </div>

  
  <div class="row gateway-links">


  
    <div class="tiny-12 small-6 columns section-a">
      
      <div class="row ">
        <div class="tiny-12 columns gateway-links-titls">
          <h3>Academics</h3>
        </div>
      </div>
      <div class="row ">
        <div class="tiny-6 small-6 columns">
          <img src="http://placehold.it/220x150"/>
          <ul class="no-bullets" class="long-links-list">
            <li>
              <div class="section-title">Academic Section 1</div>
              <ul class="no-bullets">
                <li><a href="#">Donec Et Eleifend</a></li>
                <li><a href="#">Sem Quis Vehicula</a></li>
                <li><a href="#">Ante Donec A</a></li>
                <li><a href="#">Eros Rhoncus Pharetra</a></li>
                <li><a href="#">Erat Ac Varius</a></li>
                <li><a href="#">Turpis Etiam In</a></li>
                <li><a href="#">Sagittis nunc Pellentesque</a></li>
                <li><a href="#">Nec Dapibus Nisl</a></li>
                <li><a href="#">Non Feugiat Purus</a></li>
                <li><a href="#">DOnec adipiscing Libero</a></li>
                <li><a href="#">Velit At Luctus</a></li>
                <li><a href="#">Enim Venenatis Id</a></li>
                <li><a href="#">Aliquam Urna Urna</a></li>
                <li><a href="#">Lobortis Eget Enim</a></li>
              </ul>
            </li>
            <li>
              <div class="section-title">Academic Section 2</div>
              <ul class="no-bullets">
                <li><a href="#">Vestibulum Leo Nulla</a></li>
                <li><a href="#">In Adipiscing Odio</a></li>
                <li><a href="#">Interdum Quis Praesent</a></li>
              </ul>
            </li>
          </ul>
        </div>
       
        <!-- sub col 1.1 -->
        <div class="tiny-6 small-6 columns">
          <ul class="no-bullets" class="long-links-list">
            <li>
              <div class="section-title">Academic Section 3</div>
              <ul class="no-bullets">
                <li><a href="#">Donec Et Eleifend</a></li>
                <li><a href="#">Sem Quis Vehicula</a></li>
                <li><a href="#">Ante Donec A</a></li>
                <li><a href="#">Eros Rhoncus Pharetra</a></li>
                <li><a href="#">Erat Ac Varius</a></li>
                <li><a href="#">Turpis Etiam In</a></li>
                <li><a href="#">Sagittis nunc Pellentesque</a></li>
                <li><a href="#">Nec Dapibus Nisl</a></li>
                <li><a href="#">Non Feugiat Purus</a></li>
                <li><a href="#">DOnec adipiscing Libero</a></li>
                <li><a href="#">Velit At Luctus</a></li>
                <li><a href="#">Enim Venenatis Id</a></li>
                <li><a href="#">Aliquam Urna Urna</a></li>
                <li><a href="#">Lobortis Eget Enim</a></li>
              </ul>
            </li>
            <li>
              <div class="section-title">Academic Section 4</div>
              <ul class="no-bullets">
                <li><a href="#">Vestibulum Leo Nulla</a></li>
                <li><a href="#">In Adipiscing Odio</a></li>
                <li><a href="#">Interdum Quis Praesent</a></li>
              </ul>
            </li>
          </ul>
        </div>


      </div> <!-- / double col 2 --> 

    </div>
    <div class="tiny-12 small-6 columns section-a">
      
      <div class="row ">
        <div class="tiny-12 columns gateway-links-titls">
          <h3>Student Life</h3>
        </div>
      </div>
      <div class="row ">
        <div class="tiny-6 small-6 columns">
          <img src="http://placehold.it/220x150"/>
          <ul class="no-bullets" class="long-links-list">
            <li>
              <div class="section-title">Student Section 1</div>
              <ul class="no-bullets">
                <li><a href="#">Donec Et Eleifend</a></li>
                <li><a href="#">Sem Quis Vehicula</a></li>
                <li><a href="#">Ante Donec A</a></li>
                <li><a href="#">Eros Rhoncus Pharetra</a></li>
                <li><a href="#">Erat Ac Varius</a></li>
                <li><a href="#">Turpis Etiam In</a></li>
                <li><a href="#">Sagittis nunc Pellentesque</a></li>
                <li><a href="#">Nec Dapibus Nisl</a></li>
                <li><a href="#">Non Feugiat Purus</a></li>
                <li><a href="#">DOnec adipiscing Libero</a></li>
                <li><a href="#">Velit At Luctus</a></li>
                <li><a href="#">Enim Venenatis Id</a></li>
                <li><a href="#">Aliquam Urna Urna</a></li>
                <li><a href="#">Lobortis Eget Enim</a></li>
              </ul>
            </li>
            <li>
              <div class="section-title">Student Section 2</div>
              <ul class="no-bullets">
                <li><a href="#">Vestibulum Leo Nulla</a></li>
                <li><a href="#">In Adipiscing Odio</a></li>
                <li><a href="#">Interdum Quis Praesent</a></li>
              </ul>
            </li>
          </ul>
        </div>
       
        <!-- sub col 1.1 -->
        <div class="tiny-6 small-6 columns">
          <ul class="no-bullets" class="long-links-list">
            <li>
              <div class="section-title">Student Section 3</div>
              <ul class="no-bullets">
                <li><a href="#">Donec Et Eleifend</a></li>
                <li><a href="#">Sem Quis Vehicula</a></li>
                <li><a href="#">Ante Donec A</a></li>
                <li><a href="#">Eros Rhoncus Pharetra</a></li>
                <li><a href="#">Erat Ac Varius</a></li>
                <li><a href="#">Turpis Etiam In</a></li>
                <li><a href="#">Sagittis nunc Pellentesque</a></li>
                <li><a href="#">Nec Dapibus Nisl</a></li>
                <li><a href="#">Non Feugiat Purus</a></li>
                <li><a href="#">DOnec adipiscing Libero</a></li>
                <li><a href="#">Velit At Luctus</a></li>
                <li><a href="#">Enim Venenatis Id</a></li>
                <li><a href="#">Aliquam Urna Urna</a></li>
                <li><a href="#">Lobortis Eget Enim</a></li>
              </ul>
            </li>
            <li>
              <div class="section-title">Student Section 4</div>
              <ul class="no-bullets">
                <li><a href="#">Vestibulum Leo Nulla</a></li>
                <li><a href="#">In Adipiscing Odio</a></li>
                <li><a href="#">Interdum Quis Praesent</a></li>
              </ul>
            </li>
          </ul>
        </div>


      </div> <!-- / double col 2 --> 

    </div>


  </div> <!-- / .gateway-links -->




	<div class="row page-footer align-center" >

    <?php $app->file_include('components/page_footer.php'); ?>
  </div>
