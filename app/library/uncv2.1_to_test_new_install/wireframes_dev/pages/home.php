

  
<?php 

// set subnav menu contents for this wireframe
$app->set_template_global_vars(array('topbarsubnav' => 'blocks/linklists/subnav-home'));

?>


  <!-- Masthead -->
  <?php $app->file_include('components/masthead.php'); ?>




  <!-- Main Section #1 - Slideshow & Sub-nav -->

	<div class="row fill-left fill-right collapse" id="home-splash">

		<div class="tiny-12 small-9 columns">
      <?php $app->file_include('blocks/home/slideshow.php'); ?>

      <!-- "Seriously Creative" - Desktop  -->
      <div class="seriously-creative hide-for-tiny">
        <a class="seriously_creative_wflegend"><img src="http://thedrupalteam.com/unc-wireframing/images/seriously_creative_wide.png"</img></a>
        <a class="button cta">Check Us Out!</a>
      </div>


    </div>

		<div class="tiny-12 small-3 columns sidebar-right" id="sidebar-right">
      <?php $app->file_include('blocks/home/sidenavhome.html'); ?>
      <div class="panel cta-buttons">
        <?php $app->file_include('blocks/shared/apply.php'); ?>
        <?php $app->file_include('blocks/shared/give.php'); ?>
      </div>
      <div class="text-center">
        <?php $app->file_include('blocks/shared/socmedia.php'); ?> 
      </div>
		</div>

	</div>

  <!-- "Seriously Creative" - Desktop  -->
<!--   <div class="row fill-left fill-right hide-for-tiny" >
    
    <div class="small-9 columns seriously-creative">
        <a class="seriously_creative_wflegend"><img src="http://thedrupalteam.com/unc-wireframing/images/seriously_creative_wide.png"</img></a>
        <a class="button cta">Check Us Out!</a>
    </div>
    <div class="small-3 columns home-pg-soc-media" >
      <?php $app->file_include('blocks/shared/socmedia.php'); ?>
    </div>
  </div> -->



  <!-- "Seriously Creative" - Mobile  -->
	<div class="row fill-left fill-right" id="home-content">

		<div class="small-12 columns seriously-creative">
      <div class="show-for-tiny">
        <a class="seriously_creative_wflegend"><img src="http://thedrupalteam.com/unc-wireframing/images/seriously_creative_wide.png"</img></a>
        <a class="button cta">Check Us Out!</a>
      </div>

    </div>

	</div>


	<div class="row" id="content home">

    <!-- EVENTS & NEWS -->
    <div class="tiny-12 small-5 not-tiny-push-7 large-4 large-push-8 columns content-block">
      <?php $app->file_include('blocks/home/recent_news.php'); ?>
      <?php $app->file_include('blocks/home/events.php'); ?>
    </div>

    <!-- FEATURED BLOCKS --> 
    <div class="tiny-12 small-7 not-tiny-pull-5 large-8 large-pull-4 columns content-block featured-blocks"> 
      <?php $app->file_include('blocks/home/campus_updates2.php'); ?>
		</div>


	</div>



	<div class="row page-footer align-center" >

    <?php $app->file_include('components/page_footer.php'); ?>
  </div>

