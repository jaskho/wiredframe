

      elms: [
        {sel:'#masthead',idx:1,
          notes:'<span class="name">Masthead Region</span>'
        },
        {sel:'.search-bar', idx:2, styles:{left:'-28px'},
          notes:'<span class="name">Search Bar</span>'
        },
        {sel:'.social-media-buttons', idx:3, styles:{left:'-28px'}, parentStyles:{overflow:'visible'},
          notes:'<span class="name">Social Media</span>'
        },
        {sel:'#navigation-utility', idx:4, styles:{left:'-28px'}, parentStyles:{overflow:'visible'},
          notes:'<span class="name">Utility Navigation</span>'
        },
        {sel:'.top-bar', idx:5, styles:{top:'-12px',left:'0'},
          notes:'<span class="name">Main Navigation</span>'
        },
        {sel:'.act-now', idx:6, styles:{top:'-6px',left:'0'},
          notes:'<span class="name">Optional audience-targeted promotion or call to action</span>'
        },
        {sel:'.featured-media', idx:7, styles:{top:'-6px',left:'0'},
          notes:'<span class="name">Featured video, image carousel, or other content</span>'
        },
        {sel:'.this-week-title', idx:8, styles:{top:'-6px',left:'0'},
          notes:'<span class="name">Topical, fresh content listings</span>'
        },
        {sel:'.gateway-links', idx:9, styles:{top:'-6px',left:'0'},
          notes:'<span class="name">Audience-targeted Portal Links</span>'
        },
        {sel:'.page-footer', idx:13,
          notes:'Page Footer'
        }
      ]
