<div class="row fill-left fill-right footer-blocks">
  <div class="tiny-6 small-3 columns promo-links-a">
    <p class="featured-link"><img src="http://placehold.it/230x86&text=Promo+Link"/></p>
    <p class="featured-link"><img src="http://placehold.it/230x86&text=Promo+Link"/></p>
  </div>
  <div class="tiny-6 small-3 push-6 columns promo-links-b">
    <p class="featured-link"><img src="http://placehold.it/230x86&text=Promo+Link"/></p>
    <p class="featured-link"><img src="http://placehold.it/230x86&text=Promo+Link"/></p>
  </div>
  <div class="tiny-12 small-6 pull-3 columns faculty-highlight">
    <div class="featured-link">
      <h3>Faculty Highlight</h3>
      <img src="http://placehold.it/130x130"/>
      <p>Donec faucibus, lacus eget rhoncus vehicula, augue felis tristique
      nibh, sit amet</p>
      <a href="#" class="tiny round button">Meet more of our faculty</a>
    
    </div>
  </div>
</div>
