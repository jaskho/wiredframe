<ul class="side-nav fill-top" id="sidebar-menu">
  <li><a href="">ABOUT OLLI</a></li>
  <li><a href="">PROGRAMS</a></li>
  <li><a href="">COURSES AND WORKSHOPS</a></li>
  <li><a href="">CENTER GROUPS</a></li>
  <li><a href="">MEMBERSHIP</a></li>
  <li class="hide-for-tiny"><a href="">VOLUNTEERING</a></li>
  <li class="hide-for-tiny"><a href="">REUTER CENTER</a></li>
  <li class="hide-for-tiny"><a href="">CALENDAR</a></li>
  <li class="hide-for-tiny"><a href="">PRESENTATIONS</a></li>
  <li class="hide-for-tiny"><a href="">FORMS</a></li>
  <li class="hide-for-tiny"><a href="">DIRECTIONS</a></li>
  <li class="hide-for-tiny"><a href="">ADVERSE WEATHER POLICY</a></li>
</ul>
