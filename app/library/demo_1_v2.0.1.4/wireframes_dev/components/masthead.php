<?php 
 
?>
  <div class="row fill-left collapse page-header-large" id="page-header">
    <div id="masthead" class="masthead">
      <div class="tiny-12 small-4 columns logo">
        <img src="http://placehold.it/285x65&amp;text=Logo" alt="">
      </div>
      <?php $app->file_include('blocks/shared/utility_links'); ?>
    </div>
  </div>


  <div class="row page-header-mobile">
    <div class="tiny-12 small-4 columns logo" id="logo">
      <img src="http://placehold.it/285x65&amp;text=Logo">
    </div>
  </div>

  <!-- Main Nav -->
<?php
if( ! (isset($hide_main_nav) && $hide_main_nav) ) { ?>
  <div class="row fill-left fill-right collapse main-nav" id="main-nav">
    <div class="large-12 columns">
      <?php $app->file_include('components/topnav_round2'); ?>
    </div>
  </div>
<?php } ?>

