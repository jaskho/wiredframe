
      elms: [
        {sel:'#masthead',idx:1,
          notes:'<span class="name">Masthead Region</span>'
        },
        {sel:'.search-bar', idx:2, styles:{left:'-28px'},
          notes:'<span class="name">Search Bar</span>'
        },
        {sel:'.social-media-buttons', idx:3, styles:{left:'-28px'}, parentStyles:{overflow:'visible'},
          notes:'<span class="name">Social Media</span>'
        },
        {sel:'#navigation-utility', idx:4, styles:{left:'-28px'}, parentStyles:{overflow:'visible'},
          notes:'<span class="name">Utility Navigation</span>'
        },
        {sel:'.subsite-title-bar a', idx:5, styles:{top:'-12px',left:'0'},
          notes:'<span class="name">Subsite Title and "home" link; visually salient and integrated with subsite navigation</span>'
        },
        {sel:'ul.side-nav', idx:6, styles:{right:'0'},
          notes:'<span class="name">Subsite Navigation</span>'
        },
        {sel:'.sidebar-info', idx:7, styles:{top:'-7px',left:'-13px'},
          notes:'<span class="name">Informational Content & Links</span>'
        },
        {sel:'.olli-story', idx:8,
          notes:'<span class="name">Main node content.  Larger font size for older audience.</span>'
        },
        {sel:'.upcoming-events', idx:9,styles:{top:'-7px',left:'-13px'},
          notes:'<span class="name">Dynamic content blocks</span>'
        },
        {sel:'.footer-blocks', idx:10, styles:{top:'-15px'},
          notes:'<span class="name">Promotional Footer</span>'
        },
        {sel:'.page-footer', idx:11,
          notes:'Page Footer'
        }
      ]
