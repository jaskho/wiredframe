  <div class="slideshow-wrapper">
    <div class="preloader"></div>
    <ul data-orbit data-options="timer_speed:3500">

      <li>
        <img src="http://placehold.it/720x400&text=OLLI+Slide+1" width="720" height="400" alt="Science Magnified">
        <div class="orbit-caption">
          <div class="caption-text">
            <a href="#">
						<span class="heading">Photography Composition Workshop</span><br/>
            <span class="subheading">David Simchock's 'Learn How to See the
            World' photography workshop at rest</span>
            </a>
          </div>
				</div>
      </li>

      <li>
        <img src="http://placehold.it/720x400&text=OLLI+Slide+2" width="720" height="400" alt="Science Magnified">
        <div class="orbit-caption">
          <div class="caption-text">
            <a href="#">
						<span class="heading">Professor Ruiz wins applause</span><br/>
            <span class="subheading">Hitting the high notes in piano, physics and technology</span>
            </a>
          </div>
				</div>
      </li>

      <li>
        <img src="http://placehold.it/720x400&text=OLLI+Slide+3" width="720" height="400" alt="Science Magnified">
        <div class="orbit-caption">
          <div class="caption-text">
            <a href="#">
						<span class="heading">Talk about baseball!</span><br/>
            <span class="subheading">Former Sports Illustrated executive, Larry Griswold, brings it home to OLLI</span>
            </a>
          </div>
				</div>
      </li>

    </ul> <!-- /slideshow -->
  </div>
