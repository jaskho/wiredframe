<div class="olli-main-content">
  <p class="olli-story">The Osher Lifelong Learning Institute at UNC Asheville (formerly the North
  Carolina Center for Creative Retirement) is an award-winning,
  internationally-acclaimed learning community dedicated to promoting lifelong
  learning, leadership, community service, and research. We opened our doors in
  1988 as a department of the University of North Carolina at Asheville. Our
  goal is to enable our members to thrive in life.s second half.</p>










  <h3 class="upcoming-events">UPCOMING EVENTS</h3>
  <div class="panel">
    <h4>Asheville Storytelling Circle</h4>

    <span class="event-time">July 17, 2013, 11:30 a.m.</span>

    <p>OLLI welcomes the Asheville Storytelling Circle with  "A Visit with Golda Meir" performed by RoseLynn Katz. Come to Golda.s for a bowl of chicken soup and the high.s and low.s of her remarkable journey . from a terrified child in the face of a pogram to Prime Minister of Israel and a woman way ahead of her time. The Asheville Storytelling Circle  is a non-profit organization of folks who love stories. Some of the members are professional tellers, others are amateur tellers, and still others just love listening to stories.</p>
  </div>


  <h3>PROFILES AND STORIES</h3>
  <div class="panel">
    <p><a href="">The Amazing Pilgrimage of Tom Sanders</a></p>
    <p><a href="">Teaching History with a Passion-Mary Lasher</a></p>
    <p><a href="">.But I.ll tell you a very funny story..A brief conversation with Mario A. DiCesare</a></p>
    <p><a href="">More Profiles and Stories ></a></p>
  </div>

</div>
