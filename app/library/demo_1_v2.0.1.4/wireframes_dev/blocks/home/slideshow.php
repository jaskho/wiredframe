  <div class="slideshow-wrapper">
    <div class="preloader"></div>
    <ul data-orbit data-options="timer_speed:3500">

      <li>
        <img src="http://placehold.it/720x400&text=Slide1" width="720" height="400" alt="Science Magnified">
        <div class="orbit-caption">
          <div class="caption-text">
            <a href="#">
						<span class="heading">Science Magnified</span><br/>
            <span class="subheading">Biology students scrutinize parasites up close</span>
            </a>
          </div>
				</div>
      </li>

      <li>
        <img src="http://placehold.it/720x400&text=Slide2" width="720" height="400" alt="Science Magnified">
        <div class="orbit-caption">
          <div class="caption-text">
            <a href="#">
						<span class="heading">Eyes on the Storm</span><br/>
            <span class="subheading">Students get close-up views of tornadoes, careers in meteorology</span>
            </a>
          </div>
				</div>
      </li>

      <li>
        <img src="http://placehold.it/720x400&text=Slide3" width="720" height="400" alt="Science Magnified">
        <div class="orbit-caption">
          <div class="caption-text">
            <a href="#">
						<span class="heading">Offshore Adventures in Meteorology</span><br/>
            <span class="subheading">From the tropics to Russia, Jenny Hibbert '09 puts her education into action</span>
            </a>
          </div>
				</div>
      </li>

      <li>
        <img src="http://placehold.it/720x400&text=Slide4" width="720" height="400" alt="Science Magnified">
        <div class="orbit-caption">
          <div class="caption-text">
            <a href="#">
						<span class="heading">Summer Reading</span><br/>
            <span class="subheading">The newest novels and nonfiction from faculty and alumni authors</span>
            </a>
          </div>
				</div>
      </li>

    </ul> <!-- /slideshow -->
  </div>
