
<h2 class="news-title">RECENT NEWS</h2>  
<ul class="news-list no-bullets teasers">
  <li class="">  
      <a href="#">UNC Asheville Named a .Best Buy. by Fiske Guide to Colleges</a>
  </li>
  <li class="">  
    <a href="#">Impractical Laborers Open Conference, Exhibition July 6 at Center for Craft, Creativity &amp; Design</a>
  </li>
  <li class="">  
    <a href="#">"Venezuela After Chavez" to Begin Fall World Affairs Council Series</a>
  </li>
  <li class="">  
    <a href="#">UNC Asheville Welcomes Largest Freshman Class in Four Years</a>
  </li>  
</ul>


<p><a href="#">More News &gt;</a></p>
