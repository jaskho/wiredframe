<h2 class="events-title">EVENTS</h2>
<ul class="events-list no-bullets teasers">
  <li class="">
    <div class="row">
      <div class="small-1 columns">
        <a class="calendar-graphic" href="#" >
         <span class="date-month">Jul</span> 
         <span class="date-day">3</span>
        </a> 
      </div>
      <div class="small-11 columns">
        <a class="event-details right" href="#" ><img src="http://placehold.it/60x80&text=Event+Hero"/></a>
        <a href="#"><span class="date-time">5:00 PM</span>ILSSA Exhibition Opening</a>
      </div>
    </div>
  </li>
  <li class="">
    <div class="row">
      <div class="small-1 columns">
        <a class="calendar-graphic" href="#" >
         <span class="date-month">Jul</span> 
         <span class="date-day">3</span>
        </a> 
      </div>
      <div class="small-11 columns">
        <a class="event-details" href="#" >
          <span class="date-time">7:00 PM</span>OLLI: Blue Ridge Orchestra Open Rehearsal</a>
      </div>
    </div>
  </li>
  <li class="">
    <div class="row">
      <div class="small-1 columns">
        <a class="calendar-graphic" href="#" >
         <span class="date-month">Jul</span> 
         <span class="date-day">4</span>
        </a> 
      </div>
      <div class="small-11 columns">
        <a class="event-details" href="#" >
          <span class="date-time">7:00 PM</span>Departmental Honors Reception</a>
      </div>
    </div>
  </li>
  <li class="">
    <div class="row">
      <div class="small-1 columns">
        <a class="calendar-graphic" href="#" >
         <span class="date-month">Jul</span> 
         <span class="date-day">4</span>
        </a> 
      </div>
      <div class="small-11 columns">
        <a class="event-details" href="#" >
          <span class="date-time">3:00 PM</span>Financial Aid Workshop</a>
      </div>
    </div>
  </li>
  <li class="">
    <div class="row">
      <div class="small-1 columns">
        <a class="calendar-graphic" href="#" >
         <span class="date-month">Jul</span> 
         <span class="date-day">5</span>
        </a> 
      </div>
      <div class="small-11 columns">
        <a class="event-details" href="#" >
          <span class="date-time">7:30 PM</span>New Students Reception and Dance Party</a>
      </div>
    </div>
  </li>
</ul>
<p><a href="#">More Events &gt;</a></p>
