<div class="weather">
  <div class="weather-icon-wrapper">
    <img src="/images/night-overcast.png" class="gray" alt="Overcast" title="Overcast">
  </div>
  <ul class="no-bullets">
    <li>Overcast</li>
    <li><span id="temperature"><span style="white-space:nowrap;">64.4.°F</span></span></li>
  </ul>
</div>
