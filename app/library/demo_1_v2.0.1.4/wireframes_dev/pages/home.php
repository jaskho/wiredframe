


<?php 
  // set subnav menu contents for this wireframe
  $app->set_template_global_vars(array('topbarsubnav' => 'blocks/linklists/subnav-home'));
?>


<!-- Masthead -->
<?php $app->file_include('components/masthead.php'); ?>


	<div class="row collapse">

		<div class="tiny-12 small-8 columns">


      <?php $app->file_include('blocks/home/flexslider.html'); ?>

      <!-- Apply/Give - Mobile   -->
      <div class="show-for-tiny cta-buttons cta-buttons-mobile">
        <?php $app->file_include('blocks/shared/apply-give'); /* One file to prevent extraneous whitespace breaking layout */ ?>
      </div>

      <!-- "Seriously Creative" - Desktop  -->
      <div class="seriously-creative" style="z-index:3000; position:relative;">
        <a class="seriously_creative_wflegend"><img src="http://thedrupalteam.com/unc-wireframing/images/seriously_creative_wide.png" alt="seriously creative"></a>
        <a class="button cta">Check Us Out!</a>
      </div>


      <div class="collapsed-inset-small">
        <!--  News & Events  -  Mobile  -->
        <div class="sidebar-content-mobile show-for-tiny">
          <div class="row internal-gutters">
            <?php $app->file_include('blocks/home/featured_news_item'); ?>
            <div class="clear-fix"></div>
          </div>
          <div class="row internal-gutters">
            <div class="micro-12 tiny-6 small-6 columns">
              <?php //$app->file_include('blocks/home/featured_news_item'); ?>
              <?php $app->file_include('blocks/home/recent_news.php'); ?>
            </div>
            <div class="micro-12 tiny-6 small-6 columns">
              <?php $app->file_include('blocks/home/eventsv2'); ?>
            </div>
          </div>
        </div>

        <!-- embed to add margins/padding removed by .collapse in parent col -->
        <div class="row">
          <div class="small-12 columns">
            <div class="panel hide-for-tiny">
              <?php $app->file_include('blocks/home/featured_news_item'); ?>
            </div>
            <div class="panel">
              <?php $app->file_include('blocks/home/featured_video_item'); ?>
            </div>
            <div class="panel social-media-bar">
              <?php $app->file_include('blocks/shared/socmedia.php'); ?>
              <div class="clear-fix"></div>
            </div>          
            <?php $app->file_include('blocks/home/promo_blocks'); ?>
          </div>
        </div>
      </div>




    </div>



    <!--  SIDEBAR  -  Desktop only  -->
		<div class="hide-for-tiny small-4 columns sidebar-right" id="sidebar-right">


        <?php $app->file_include('blocks/home/sidenavhome.html'); ?>
        <div class="panel cta-buttons">
          <?php $app->file_include('blocks/shared/apply.php'); ?>
          <?php $app->file_include('blocks/shared/give.php'); ?>

        </div>
        <div class="text-center">
           
        </div>

      <!-- embed to add margins/padding removed by .collapse in parent col -->
      <div class="row">
        <div class="small-12 columns">
          <?php $app->file_include('blocks/home/recent_news.php'); ?>
          <?php $app->file_include('blocks/home/eventsv2'); ?>
        </div>
      </div>

		</div>

	</div>












	<div class="row page-footer align-center" >

    <?php $app->file_include('components/page_footer_fat'); ?>
  </div>

