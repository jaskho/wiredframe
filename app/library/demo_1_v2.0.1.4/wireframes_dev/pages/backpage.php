<?php 
  // set subnav menu contents for this wireframe
  $app->set_template_global_vars(array('topbarsubnav' => 'blocks/linklists/subnav-backpage'));
?>

  <!-- Masthead -->
  <?php $app->file_include('components/masthead.php'); ?>


  <!-- Main Section   -->

	<div class="row fill-right">

		<div class="tiny-12 small-9 columns">
      <div class="page-title"><span>About a Compelling Subject</span>

    </div>
      <?php $app->file_include('blocks/backpage/main_content_backpage.php'); ?>

      <div class="panel social-media-bar">
        <?php $app->file_include('blocks/shared/socmedia.php'); ?>
        <div class="clear-fix"></div>
      </div>          

    </div>

		<div class="tiny-12 small-3 columns" id="sidebar-right">
      <div class="hide-for-tiny">
        <?php $app->file_include('blocks/backpage/secondary_menu_backpage.php'); ?>
      </div>
      <?php $app->file_include('blocks/backpage/sidebar_backpage.php'); ?>
		</div>

	</div>



	<div class="row page-footer align-center" >

    <?php $app->file_include('components/page_footer.php'); ?>
  </div>







