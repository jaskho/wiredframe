


<style>

img {
  background-color: black;
}
img:hover {
  opacity: 0.5;
}
.no-f-no-t img {
  
}

.trans-only img {
  /*-webkit-transition: background-color 300ms ease-out;
  -moz-transition: background-color 300ms ease-out;
  transition: background-color 300ms ease-out;*/

  -webkit-transition: opacity 300ms ease-out;
  -moz-transition: opacity 300ms ease-out;
  transition: opacity 300ms ease-out;  
}

.filter-only img {
  -webkit-filter: grayscale(100%) opacity(0.5);
  -moz-filter: grayscale(100%) opacity(0.5);
  filter: grayscale(100%) opacity(0.5);
  filter: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id…0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale"); 
}

.filter-and-trans img {
  -webkit-transition: opacity 300ms ease-out;
  -moz-transition: opacity 300ms ease-out;
  transition: opacity 300ms ease-out;  
  
  -webkit-filter: grayscale(100%) opacity(0.5);
  -moz-filter: grayscale(100%) opacity(0.5);
  filter: grayscale(100%) opacity(0.5);
  filter: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id…0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale"); 
  
}

</style>
	<div class="row ">

		<div class="small-12 columns">



      <div class="panel social-media-bar no-f-no-t">
        <h3>No filter, no transition</h3>
        <img src="/unc-wireframing/images/night-overcast.png" class="" alt="Overcast" title="Overcast">
      </div> 

      <div class="panel social-media-bar trans-only">
        <h3>Transition only</h3>
        <img src="/unc-wireframing/images/night-overcast.png" class="" alt="Overcast" title="Overcast">
      </div> 

      <div class="panel social-media-bar filter-only">
        <h3>Filter only</h3>
        <img src="/unc-wireframing/images/night-overcast.png" class="" alt="Overcast" title="Overcast">
      </div> 

      <div class="panel social-media-bar filter-and-trans">
        <h3>Filter and transition</h3>
        <img src="/unc-wireframing/images/night-overcast.png" class="" alt="Overcast" title="Overcast">
      </div> 






    </div>









	</div>












	<div class="row page-footer align-center" >

    <?php $app->file_include('components/page_footer_fat'); ?>
  </div>

