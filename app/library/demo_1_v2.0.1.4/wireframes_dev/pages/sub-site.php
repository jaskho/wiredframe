<?php

// set subnav menu contents for this wireframe
$app->set_template_global_vars(array('topbarsubnav' => 'blocks/linklists/subnav_math.html'));
?>





  <!-- Masthead -->
  <?php $app->file_include('components/masthead.php'); ?>
  <?php $app->file_include('blocks/sub-site/subsite-section-bar.php'); ?>
  

  <div class="row show-for-tiny mobile-sub-nav">
    <div class="tiny-12 columns">
      <?php $app->file_include('blocks/sub-site/sidenav_regular_math.html'); ?>
    </div>
  </div>




  <!-- Main Content Column -->

	<div class="row fill-right">

		<div class="tiny-12 small-9 columns">
      <div class="page-title">Welcome</div>
      <?php $app->file_include('blocks/sub-site/main_content_math.html'); ?>

      <div class="panel social-media-bar">
        <?php $app->file_include('blocks/shared/socmedia.php'); ?>
        <div class="clear-fix"></div>
      </div>          


      <?php $app->file_include('blocks/sub-site/featured_blocks.html'); ?>

    </div>

		<div class="tiny-12 small-3 columns hide-for-tiny" id="sidebar-right">
      <div class="hide-for-tiny">
        <?php $app->file_include('blocks/sub-site/sidenav_regular_math.html'); ?>
      </div>
      <?php $app->file_include('blocks/sub-site/sidebar_math.php'); ?>
		</div>

	</div>


  <?php $app->file_include('blocks/sub-site/subsite_footer_blocks.php'); ?>

  <!-- Contact info -- mobile -->
  <div class="row show-for-tiny">
    <div class="small-12 columns">
      <?php $app->file_include('blocks/sub-site/sidebar_math.php'); ?>
    </div>
  </div>

	<div class="row page-footer align-center" >
    <?php $app->file_include('components/page_footer.php'); ?>
  </div>







