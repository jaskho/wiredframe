


<?php $app->hide_main_nav = true; ?>



  <!-- Masthead -->
  <?php $app->file_include('components/masthead.php', array('hide_main_nav' => true)); ?>

  <?php $app->file_include('blocks/sub-site/subsite-section-bar-olli.php'); ?>

  <div class="row show-for-tiny mobile-sub-nav">
    <div class="tiny-12 columns">
      <?php $app->file_include('blocks/shared/sidenav_regular.php'); ?>
    </div>
  </div>

  <!-- Main Section #1 - Slideshow & Sub-nav -->

	<div class="row fill-right">

		<div class="tiny-12 small-9 columns">
      <div class="page-title">Welcome</div>
      <?php $app->file_include('blocks/sub-site/slideshow_subsite.php'); ?>
      <?php $app->file_include('blocks/sub-site/main_content_olli.php'); ?>
    </div>

		<div class="tiny-12 small-3 columns" id="sidebar-right">
      <div class="hide-for-tiny">
        <?php $app->file_include('blocks/shared/sidenav_regular.php'); ?>
      </div>
      <?php $app->file_include('blocks/sub-site/sidebar_olli.php'); ?>
		</div>

	</div>


  <?php $app->file_include('blocks/sub-site/subsite_footer_blocks.php'); ?>

	<div class="row page-footer align-center" >

    <?php $app->file_include('components/page_footer.php'); ?>
  </div>







