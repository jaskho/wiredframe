This is a fully functioning wireframing project, demonstrating various features and techniques.

NOTE: it is built and tested against Wiredframe v2.0.1.4

To deploy:
1. GIT clone or otherwise install Wiredframe v2.0.14
2. If necessary, delete or move the Wiredframe GIT repo out of the installation folder.
3. Copy all of the files within this directory to the root of your Wiredframe application install.
4. Optionally, initialize a GIT repo for your project.
  A. Best practice is to implement source control for your project separately from the Wiredframe repo.
  B. Rename sample.gitignore to .gitigore (this will keep your project repo clean, ignoring Wiredframe application files/folders).