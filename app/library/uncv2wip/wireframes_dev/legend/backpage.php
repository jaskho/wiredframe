
      elms: [
        {sel:'#masthead',idx:1,
          notes:'<span class="name">Masthead Region</span>'
        },
        {sel:'.search-bar', idx:2, styles:{left:'-28px'},
          notes:'<span class="name">Search Bar</span>'
        },
        {sel:'.social-media-buttons', idx:3, styles:{left:'-28px'}, parentStyles:{overflow:'visible'},
          notes:'<span class="name">Social Media</span>'
        },
        {sel:'#navigation-utility', idx:4, styles:{left:'-28px'}, parentStyles:{overflow:'visible'},
          notes:'<span class="name">Utility Navigation</span>'
        },
        {sel:'.top-bar', idx:5, styles:{top:'-12px',left:'0'},
          notes:'<span class="name">Main Navigation</span>'
        },
        {sel:'.share-this', idx:6,
          notes:'<span class="name">"Share this page" links</span>'
        },
        {sel:'.full-width-image', idx:7,
          notes:'<span class="name">Example full-width image</span>'
        },
        {sel:'img.right', idx:8,targetParent:true, styles:{right:'200px', left:'auto'},
          notes:'<span class="name">Example spot image, floated right</span>'
        },
        {sel:'img.left', idx:9, targetParent:true,
          notes:'<span class="name">Example spot image, floated left</span>'
        },
        {sel:'.page-footer', idx:10,
          notes:'Page Footer'
        }
      ]
