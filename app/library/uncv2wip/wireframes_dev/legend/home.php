
      elms: [
        {sel:'#masthead',idx:1,
          notes:'<span class="name">Masthead Region</span>'
        },
        {sel:'.search-bar', idx:2, styles:{left:'-28px'},
          notes:'<span class="name">Search Bar</span>'
        },
        {sel:'.social-media-buttons', idx:3, styles:{left:'-28px'}, parentStyles:{overflow:'visible'},
          notes:'<span class="name">Social Media</span>'
        },
        {sel:'#navigation-utility', idx:4, styles:{left:'-28px'}, parentStyles:{overflow:'visible'},
          notes:'<span class="name">Utility Navigation</span>'
        },
        {sel:'.top-bar', idx:5, styles:{top:'-12px',left:'0'},
          notes:'<span class="name">Main Navigation</span>'
        },
        {sel:'.slideshow-wrapper', idx:6,
          notes:'<span class="name">Carousel</span>'
        },
        {sel:'#sidebar-menu', idx:7,
          notes:'<span class="name">Secondary Navigation</span>'
        },
        {sel:'.cta-buttons', idx:8,
          notes:'<span class="name">Call to Action</span>'
        },
        {sel:'.seriously-creative', idx:9,
          notes:'<span class="name">Promotional Area</span>'
        },
        {sel:'.featured-blocks', idx:10, styles:{top:'-7px'},
          notes:'<span class="name">Featured Content Region</span>'
        },
        {sel:'.fb1', idx:'10.1', styles:{top:'20px'},
          notes:'<span class="name">Feature Block A</span>'
        },
        {sel:'.fb1 img.hero', idx:'10.2', styles:{left:'inherit',right:'78px'},targetParent:true,
          notes:'<span class="name">Example of block with hero image</span>'
        },
        {sel:'.fb2', idx:'10.3',
          notes:'<span class="name">Feature Block B</span>'
        },
        {sel:'.fb2 .video', idx:'10.4', targetParent:true,
          notes:'<span class="name">Example of block with video content</span>'
        },
        {sel:'.fb3', idx:'10.5',
          notes:'<span class="name">Feature Block C</span>'
        },
        {sel:'.fb4', idx:'10.6',
          notes:'<span class="name">Feature Block D</span>'
        },
        {sel:'.events-title', idx:11, styles:{left:'inherit',right:'5px'},
          notes:'<span class="name">Events Teaser Listing</span>'
        },
        {sel:'.events-list img', idx:'11.1', sstyles:{left:'inherit',right:'25px'},targetParent:true,
          notes:'<span class="name">Example of even with hero image</span>'
        },
        {sel:'.news-title', idx:12, styles:{left:'inherit',right:'5px'},
          notes:'<span class="name">News Teaser Listing</span>'
        },
        {sel:'.page-footer', idx:13,
          notes:'Page Footer'
        }
      ]
