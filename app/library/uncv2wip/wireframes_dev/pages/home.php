





  <!-- Masthead -->
  <?php $app->file_include('components/masthead.php'); ?>




  <!-- Main Section #1 - Slideshow & Sub-nav -->

	<div class="row fill-left fill-right collapse" id="home-splash">

		<div class="tiny-12 small-9 columns">
      <?php $app->file_include('blocks/slideshow.php'); ?>
    </div>

		<div class="tiny-12 small-3 columns" id="sidebar-right">
      <?php $app->file_include('blocks/sidenavhome.php'); ?>
      <div class="panel cta-buttons">
        <?php $app->file_include('blocks/apply.php'); ?>
        <?php $app->file_include('blocks/give.php'); ?>
      </div>
		</div>

	</div>



  <!-- Main Section #2 -->
	<div class="row fill-left fill-right" id="home-content">

		<div class="_tiny-12 _small-3 large-12 columns seriously-creative">
      <div class="show-for-small show-for-small-down">
        <a class="seriously_creative_wflegend"><img src="http://thedrupalteam.com/unc-wireframing/images/seriously_creative_narrow.png"</img></a>
        <a class="button cta">Check Us Out!</a>
      </div>
      <div class="hide-for-small">
        <a class="seriously_creative_wflegend"><img src="http://thedrupalteam.com/unc-wireframing/images/seriously_creative_wide.png"</img></a>
        <a class="button cta">Check Us Out!</a>
      </div>
    </div>

	</div>




  <!-- Main Section #3 -->
	<div class="row" id="content home">

    <!-- FEATURED BLOCKS --> 
    <div class="tiny-12 small-9 large-8 columns content-block featured-blocks"> 
      <?php $app->file_include('blocks/campus_updates2.php'); ?>
		</div>

    <!-- EVENTS & NEWS -->
		<div class="tiny-12 small-3 large-4 columns content-block">
      <?php $app->file_include('blocks/events.php'); ?>
      <?php $app->file_include('blocks/recent_news.php'); ?>
    </div>


	</div>



	<div class="row page-footer align-center" >

    <?php $app->file_include('components/page_footer.php'); ?>
  </div>

