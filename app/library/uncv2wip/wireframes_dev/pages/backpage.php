

  <!-- Masthead -->
  <?php $app->file_include('components/masthead.php'); ?>


  <div class="row show-for-tiny mobile-sub-nav">
    <div class="tiny-12 columns">
      <?php $app->file_include('blocks/secondary_menu_backpage.php'); ?>
    </div>
  </div>

  <!-- Main Section   -->

	<div class="row fill-right">

		<div class="tiny-12 small-9 columns">
      <div class="page-title"><span>About a Compelling Subject</span>

      <div class="share-this">
        Share 
        <img src="http://w.sharethis.com/images/facebook_32.png" class="gray"/>
        <img src="http://w.sharethis.com/images/twitter_32.png" class="gray"/>
        <img src="http://w.sharethis.com/images/googleplus_32.png" class="gray"/>
      </div>
    </div>
      <?php $app->file_include('blocks/main_content_backpage.php'); ?>
    </div>

		<div class="tiny-12 small-3 columns" id="sidebar-right">
      <div class="hide-for-tiny">
        <?php $app->file_include('blocks/secondary_menu_backpage.php'); ?>
      </div>
      <?php $app->file_include('blocks/sidebar_backpage.php'); ?>
		</div>

	</div>



	<div class="row page-footer align-center" >

    <?php $app->file_include('components/page_footer.php'); ?>
  </div>







