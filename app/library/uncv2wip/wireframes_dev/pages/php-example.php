





  <!-- Masthead -->
  <?php $app->file_include('components/masthead.php'); ?>

  <!-- Body -->
	<div class="row">

		<div class="tiny-12 small-9 columns">
      <?php $app->file_include('blocks/main_content__example'); ?>
    </div>

		<div class="tiny-12 small-3 columns">
      <?php $app->file_include('blocks/sidebar__example.php'); ?>
		</div>

	</div>

  <!-- Footer -->
	<div class="row page-footer align-center" >
    <?php $app->file_include('components/page_footer.php'); ?>
  </div>

