<ul class="side-nav fill-top" id="sidebar-menu">
  <li><a href="">Mission and Student Learning Outcomes</a></li>
  <li><a href="">Our Faculty and Staff</a></li>
  <li><a href="">For Students</a></li>
  <li><a href="">Math Lab</a></li>
  <li><a href="">About Our Students</a></li>
  <li class="hide-for-tiny"><a href="">Curriculum</a></li>
  <li class="hide-for-tiny"><a href="">Projects and Partnerships</a></li>
  <li class="hide-for-tiny"><a href="">Parsons Lecture</a></li>
  <li class="hide-for-tiny"><a href="">Department Catalog</a></li>
</ul>
