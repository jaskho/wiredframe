<img class="right" src="http://placehold.it/340x226"/>

<p>The Department of Mathematics at UNC Asheville is an engaged and exciting department located on the third floor of Robinson Hall dedicated to excellence in undergraduate teaching.  Our classes are small and are taught by professional instructors.  Students in the Math Department receive individualized attention.  The professors have an open-door policy, and students are free to meet with a professor whenever he/she is available.  We strive to foster an atmosphere that will make all students' mathematics education experience most rewarding. <a href="#">For more on this see the open letter from our department chair</a>.</p>

<p>The third floor of Robinson also houses the Mathematics Assistance Center, AKA the Parsons Math Lab. This unique facility is a focal point for the Math Department. Students can get assistance from other students or faculty. They can meet with a study group, use one of the computers with specialized software, or just bring their lunch and gaze at the scenery from above the campus quadrangle.</p>

<h2>Why Math?</h2>

<p>Mathematics is fundamental to many disciplines and is an important part of a liberal arts education. It is highly respected for developing skills which are increasingly vital to modern professions; skills such as analytical thinking, data analysis, mathematical modeling, pattern recognition, and problem solving.  We offer four concentrations: pure mathematics, applied mathematics, teaching of mathematics, and statistics. For more infomation see our curriculum page.</p>

<div class="row featured-blocks">
  <div class="tiny-12 small-6 columns">
    <div class="panel">
      <h3>PARSONS LECTURE</h3>
      <p class="subtitle">Celebrating exceptional teaching at UNC Asheville - past, present, and future.</p>
      <img class="right" src="http://placehold.it/100x67"/>
      <p>Photograph of Joe ParsonsThe Parsons Lecture invites a nationally renowned mathematician to UNC Asheville annually for a presentation on a topic accessible to the general audience.  <a href="#">Read more ></a></p>
    </div>
  </div>
  <div class="tiny-12 small-6 columns">
    <div class="panel">
      <h3>JOE PARSONS MATH LAB</h3>
      <p class="subtitle">Quick facts about the Math Lab</p>
      <ul>
        <li>Extra help in Math provided free to all UNC Asheville students.</li>
        <li>A drop-in service, no appointment is necessary.</li>
        <li>Extensive hours of operation.  <a href="#">More Information</a></li>
      </ul>
    </div>
  </div>
</div>

