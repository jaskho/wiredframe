


<p class="full-width-image"><img src="http://placehold.it/690x400&text=Full+Width+Image"   ></p>


<h2><a  href="#" >Residence Halls</a></h2>

<p>Residence Hall rooms are furnished with single beds, dressers, chairs, window blinds and Internet connection and cable television service. The common areas include laundry facilities, computer labs, TV lounges, vending machines, and study space.</p>


<h2><a  href="#" >Dining Services</a></h2>

<p><img src="http://placehold.it/250x210&text=Spot+Image" class="right"/>Come to campus hungry! A range of options await students (and visitors) including a traditional dining hall with comfort food, vegetarian and vegan specialties.&nbsp; Or you can grab a quick bites and awesome pastries in Caféamsey right inside Ramsey Library. Students can avoid the "Freshman Fifteen" by looking for healthy choices, which are clearly marked at all dining facilities. And with no fast-food chain restaurants on campus, it.s easier to eat healthy.</p>


<h2><a  href="#" >Student Health Services</a></h2>

<p>Want to know more about how to stay healthy on campus?&nbsp;Need a flu shot or think you have an ear infection? Students can visit Student Health Services for free or low-cost health care on weekdays when classes are in session. Professional mental health services also are available.&nbsp;</p>


<h2>Physical Therapy and Free Injury Screening</h2>

<p>For students, faculty and staff, Mission Rehab offers Physical Therapy and Free Injury Screens by appointment in the Athletic Training Room of the Justice Center. Call 828.251.6480 to set up appointments.</p>


<h2><img src="http://placehold.it/250x210&text=Spot+Image" class="left"/><a  href="#" >Recreation</a></h2>

<p>Keeping healthy also means staying active. Campus Recreation provides a range of ways to do just that, including an indoor pool and track, weight room, intramural sports and fitness classes. UNC Asheville's 360-acre campus is just minutes away from thousands of acres of federal and state forests where students can hike, camp, kayak and rock climb with the popular Outdoor Recreation Program. Need equipment? Students can rent outdoor gear and bikes for free or just a few bucks.</p>


<h2><a  href="#" >Transportation</a></h2>

<p>Getting around campus on foot, by bike or by campus BARCS (shuttle) is easy because the campus is relatively compact.&nbsp; And taking in the sights and sounds of downtown is just as easy by catching a ride on the Asheville Transit buses - - free for UNC Asheville students.&nbsp; For a look at all your transportation alternatives including carpooling, rental cars, shuttles to the airport and much more, visit the <a  href="#" >Transportation</a> Web site.</p>


<h2><a  href="#" >Campus Police</a></h2>

<p>UNC Asheville's Campus Police really are the "good guys." Aside from the usual policing duties, they also provide bicycle and valuable property registration for students; assistance for stranded campus motorists, including key lock outs and battery jump starts; and safety and non-emergency medical escorts for students.</p>



