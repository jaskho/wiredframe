<div class="panel fb1">
  <a href="#" class="right"><img src="http://placehold.it/120x120&text=Hero" class="right hero"/></a>
  <a href="#"><h3 class="column-title">Football &amp; Volleyball Camps Start Soon</h3></a>
  <p> UNC Asheville.s Sports Academy summer camps in football and volleyball get under way soon. Football camp, led 
  by Coach Reid Bennett of the Carolina Gladiators, begins July 15. Volleyball camp, led by Bulldog 
  Head Coach Frederico Santos, begins July 22. <a href="#">Read More &gt;</a></p>
  <div class="clear-fix"></div>
</div>

<div class="panel fb2">
  <a href="#"><h3 class="column-title">Video: Last Weedend's Folk Dance Revival</h3></a>
  <a href="#" class="left"><img src="http://placehold.it/300x200&text=Video" class="left video"/></a>
  <p class="special">For an instant in the flashing eyes of the mate, and his fiery cheeks, you would have almost 
  thought that he had really received the blaze of the levelled tube. 
  </p>
  <div class="clear-fix"></div>
</div>

<div class="row">
  <div class="large-6 columns">
    <div class="panel fb3">
      <a href="#"><h3 class="column-title">Welcome to NEH Summer Institute</h3></a>
      <a href="#" class="right"><img src="http://placehold.it/400x80&text=Banner" class="banner"/></a>
      <p> UNC Asheville is hosting the National Endowment for the Humanities 2013 Summer Institute for K-12 
      teachers, .The Power of Place: Land and Peoples in Appalachia,. July 8-26. 
      <a href="#">Read More &gt;</a></p>
    </div>
  </div>
  
  <div class="large-6 columns">
    <div class="panel fb4">
      <a href="#"><h3 class="column-title">Community Days Kick-off</h3></a>
      <p> UNC Asheville is hosting the National Endowment for the Humanities 2013 Summer Institute for K-12 
      teachers, .The Power of Place: Land and Peoples in Appalachia,. July 8-26. 
      Dan Pierce and Erica Abrams Locklear of UNC Asheville.s faculty, and Jamie Ross, producer of 
      the PBS series, Appalachia: A History of Mountains and People, are core faculty for the institute. 
      <a href="#">Read More &gt;</a></p>
    </div>
  </div>

</div>
