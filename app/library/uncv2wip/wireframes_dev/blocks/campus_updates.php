
      <h2>CAMPUS UPDATES</h2> 
			<div class="row">
				<div class="large-6 columns">
          <h3 class="column-title">Football &amp; Volleyball Camps Start Soon</h3>
          <p> UNC Asheville.s Sports Academy summer camps in football and volleyball get under way soon. Football camp, led 
          by Coach Reid Bennett of the Carolina Gladiators, begins July 15. Volleyball camp, led by Bulldog 
          Head Coach Frederico Santos, begins July 22. <a href="/campus-updates/football-volleyball-camps">Read More &gt;</a></p>
				</div>
				<div class="large-6 columns">
          <h3 class="column-title">Welcome to NEH Summer Institute</h3>
          <p> UNC Asheville is hosting the National Endowment for the Humanities 2013 Summer Institute for K-12 
          teachers, .The Power of Place: Land and Peoples in Appalachia,. July 8-26. 
          Dan Pierce and Erica Abrams Locklear of UNC Asheville.s faculty, and Jamie Ross, producer of 
          the PBS series, Appalachia: A History of Mountains and People, are core faculty for the institute. 
          <a href="/campus-updates/neh-summer-institute">Read More &gt;</a></p>
				</div>
			</div>
