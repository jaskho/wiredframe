#Overview

Wiredframe is a platform for quickly creating responsive, interactive in-browser prototypes, with an emphasis on supporting

+    Rapid, iterative development 
+ Collaboration among developers and with project stakeholders
+ Code re-use
+ Flexibility and extensibility


###PLEASE NOTE 

1.  This code is pre-alpha.  It works and is in active use in professional shops, but some features are only partially implemented (noted below), and usage patterns and APIs are not stable.
2.    Documentation: A significant refactoring has obsoleted the previous configuration and usage documentation.  Revised documentation is on its way.  In the meantime, please feel free to contact me via my bithub account for assistance. 




#Features

+ Re-usable layouts, code and content elements
    + Some starter libraries provided
    + Build libraries by importing code objects 
    + Save projects and project components as libraries, or as additions to existing ones
+ Flexible & modular templating system
    + Code
        + Write in straight html, twig or php
        + Simple API for integrating other templating systems
        + Integrated debugging framework
    + Build & Populate – pull in dummy text and media content
        + Re-use content (eg., for menus/lists or other recurring elements)
        + Dynamically generate placeholder images using simple template tags
    + Organize
        + Break UI and content elements into arbitrary blocks for modularity and manageability
        + Use template “inheritance” to explore layout options while keeping it all nice and DRY
+ Static code export – save prototypes as static html + css + javascript for simple versioning, to create alternative implementations, and for re-use in production (ie., use your mockup code as the base for building out your project)
+ Demoing tools
    + Present a read-only, but still fully interactive, view of your work with a simple link
    + Annotations—Label design elements for clear communication, with cross-references to explanatory/supplementary information.  Use toggle-able overlay to label features and add explanatory/supplementary information
+ Build using your favorite frameworks and tools
    + Framework agnostic – use whatever CSS/JS framework(s) suit your needs
    + Some built-ins to get you started – the default configuration is tailored for use with the Foundation UI framework, Sass, and CoffeeScript. (You’re in no way locked into using them, however)
    + It’s just a bunch of code – use whatever IDE and tools you prefer
+ Themeable/Brandable UI (Hey, it’s open source – do whatever you want!)
+ Easy deployment with minimal dependencies – a web server with PHP is all you need. 
+ Modular and robust architecture – Symfony2 under the hood (increasingly; see below)

###The Blue Sky Future™  

(These features are in “active development”, meaning, well, who can really say?)

+ Complete migration to Symfony2 framework
+ Enhanced admin UI
    +    Create/manage projects & project components
    +    Create/manage libraries and dependencies
    +    Annotation/overlay editing
+    Access control for teams and roles 
+    Deployment tools – set up your wireframing platform using Composer 
+    Enhanced theming API for admin/demo components
+    Integrated unit and end-to-end testing.  Full coverage for the platform and example code; stubs for your brilliant project. 
+    API for integration with third-party or custom content sources, eg. lipsum.com
+    Port to NodeJS (using the express.js framework)
+    Joyride-style demos

###The Blue Sky Future™, *with castles in it* 
   
(These features are in “fantasy development”, meaning, well… dreaming is fun)

+    Drag/drop wireframe construction 
+    Installation wizard
+    Web-based wireframing service

#Requirements
+ Base
    +    Web server with PHP (>= 3.3) 
    +    Note, no database is required
+ With support for starter/example libraries 
    +    Compass
    +    Zurb Foundation



#Installation
1.  See the requirements
2.  Clone or copy the framework to your webserver
2.  If you feel like it, do some configuration (see below)
3.  Done!

#Configuration 
__Documentation is on its way.  In the meantime, please feel free to contact me via my bithub account for assistance.__

#Usage
__Documentation is on its way.  In the meantime, please feel free to contact me via my bithub account for assistance.__

#Licensing
This software is copyright Chris Struble, made available for free use under the terms of the MIT license.



